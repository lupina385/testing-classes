from helper import Book
import random
import os

def readFile(name, dir=os.getcwd()):
    try:
        print(dir)
        f = open(dir + '\\' + name + '.txt', 'r', encoding='utf-8')
        books = []
        for x in f:
            x = x.strip()
            books.append(Book(x.split(', ', 1)[0], x.split(', ', 1)[len(x.split(', ', 1)) - 1]))
        f.close()
        return books
    except:
        return 'Zła nazwa pliku!'

def addISBN(books):
    isbn = []
    while len(isbn) < len(books):
        x = random.randint(1000000000000, 9999999999999)
        if x in isbn:
            continue
        else:
            isbn.append(x)
    for book, x in zip(books, isbn):
        book.setIsbn(x)
    return books

def booksOfN(name, n):
    books=readFile(name)
    while len(books)>n:
        x=random.randint(0, len(books)-1)
        if books[x] in books:
            books.remove(books[x])
        else:
            continue
    return addISBN(books)

