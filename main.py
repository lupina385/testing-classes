from helper import Human
from fileHandler import booksOfN
import random

if __name__=='__main__':
    books=booksOfN('lista', 10)
    anna=Human('anna', 'dzwon')
    anna.changeAge(22)
    jan=Human()
    jan.changeAge(69)
    [jan.addFavouriteBook(books[i]) for i in range(0, len(books)-1, random.randint(1, len(books)-1))]
    jan.printData()
