class Human:
    age=0
    def __init__(self, name='john', lastName='doe'):
        self.name=name
        self.lastName=lastName

    def changeAge(self, age):
        self.age=age

    def canIBuyAlcohol(self):
        if self.age<18:
            return False
        else:
            return True

    favouriteBooks=[]
    def addFavouriteBook(self, book):
        self.favouriteBooks.append(book)

    def deleteFavouriteBook(self, book):
        if book in self.favouriteBooks:
            self.favouriteBooks.remove(book)
        else:
            print(f'{self.name} nie miał takiej książki na liście jego ulubionych.')

    def printData(self):
        print('======================================')
        print(self.name)
        print(self.lastName)
        print(self.age)
        print('Ulubione książki:')
        for book in self.favouriteBooks:
            book.printData()
        print('======================================')

class Book:
    def __init__(self, name, author):
        self.name=name
        self.author=author

    def setIsbn(self, isbn):
        self.isbn=isbn

    def printData(self):
        print(self.name+', '+self.author+', '+str(self.isbn))